# Progettazione del Worker Gantt all'interno di un software per il controllo di alto livello dell'operatività aziendale di BitBoss.
Questo è un test che sottoponiamo a chi vuole candisarsi per il [tirocinio da Full Stack Ux Designer in BitBoss.](https://www.bitboss.it/jobs/ux-designer)

**Nota:** L'oggetto di questo esercizio è la progettazione di una funzionalità che abbiamo già sviluppato e stiamo utilizzando su [https://dot.bitboss.it/](https://dot.bitboss.it/).  
L'abbiamo presa come esempio per capire come risolveresti un problema di progettazione che abbiamo affrontato recentemente.

### Contesto
DOT è lo strumento che abbiamo sviluppato per controllare l'operatività di BitBoss. Ci permette programmare l'allocazione dei vari membri del team sui vari progetti, controllare i costi e tante altre cose.
### Indice
- [Informazioni a disposizione](#informazioni-a-disposizione)
- [Obiettivi](#obiettivo)
- [Vincoli](#vincoli)
- [Output atteso](#output-atteso)
- [Domande](#domande)
- [Perchè lo facciamo](#perche-lo-facciamo)
- [Non puoi essere un Tirocinante?](#non-puoi-essere-un-tirocinante)


## Informazioni a disposizione

**Worker**: è un membro del team di BitBoss.

**Accordo:** per ogni membro del team abbiamo un accordo con BitBoss. Dato un intervallo di tempo, sappiamo quante ore/settimana lavorerà con noi e a quale costo orario (lordo). Un worker può avere più accordi, ma questi non possono essere sovrapposti nel tempo.

**Progetto:** in Bitboss un progetto è normalmente un software che sviluppiamo per un nostro cliente o per noi stessi

**Programmazione:** è l'assegnazione di un worker ad un progetto per un determinato periodo di tempo e per un determinato quantitativo di ore settimanali. Un worker può essere assegnato a più progetti contemporaneamente, compatibilmente con la sua disponibilità settimanale.

**Note:** non tutti i worker sono full time (40h/settimanali). Alcuni lavorano con noi 20h/settimana  altri 30, ecc ecc.
Come unità di ragionamento utilizziamo la settimana, non il giorno. Non ci interessa (in questo contesto) avere una visibilità dell'allocazione di una persona sul singolo giorno.

## Obiettivo

Creare una pagina che ci restituisca, in maniera immediata, una visione di insieme dell'allocazione futura dei vari membri del team di BitBoss sui vari progetti.

A colpo d'occhio dovremo essere in grado di capire, per ogni singolo worker
- Quanto tempo ha a disposizione (non allocato)?
- Quante ore settimanali il worker dedica a Bitboss
- Quando termina un accordo.

Con gerarchia più bassa, dovremo anche poter capire
- Su quali progetti è allocato nel tempo e, per ognuno di questi, per quante ore settimanali.
- Per ogni settimana, come è distribuito il suo tempo.

### Obiettivo di Business

Per chi offre servizi con il modello di business della consulenza, il goal è la completa allocazione delle risorse sui vari progetti. 

Questo strumento deve fornire un supporto per la comprensione e la condivisione dello stato di allocazione dei worker nel tempo, per poter supportare le attività di chi si occupa di sales o accounting.

## Vincoli

Dot è un o strumento ad uso interno, che viene utilizzato da persone consapevoli e formate. Non viene utilizzato dal pubblico e non deve necessariamente essere comprensibile a chiunque in maniera immediata.

Essendo uno strumento interno, l'obiettivo dev'essere quello di massimizzare il rapporto tra il valore che questa funzionalità porta in BitBoss e il tempo necessario per svilupparla. Un Gantt può essere fatto in mille modi diversi. Ogni tipo di complessità nella struttura, nelle animazioni o nelle interazioni fa sicuramente crescere il tempo necessario allo sviluppo, ma non necessariamente accresce anche l'utilità generale della funzionalità.

## Output atteso

Su questo fronte hai totale libertà.

Può essere una descrizione testuale, uno sketch fatto a mano, un wireframe o, se hai competenze in ambito ui design, un prototipo super dettagliato. 

Non vogliamo farti sprecare tempo, quindi quando pensi che ciò che hai in mente sia comprensibile, **apri una nuova ISSUE qui su GitLab (ricordati di flaggarla come privata) e inviala.**

Apprezzeremo enormemente una spiegazione del perché sono state effettuate delle scelte e del perché non ne sono state effettuate delle altre.

Non aver paura di cercare fonti di ispirazione, di prendere spunto da prodotti esistenti. La capacità di cercare e capire come altri hanno risolto problemi simili è fondamentale.

Puoi linkare materiali ed effettuare l'upload di file. Carica anche il tuo Cv e qualunque cosa reputi utile.

## Domande

Se ritieni che le informazioni che abbiamo dato non siano sufficienti o non hai compreso alcune cose, invia le tue domande all'indirizzo jobs@bitboss.it. 

Fare domande giuste è una colonna portante del processo di progettazione di una funzionalità, ma quando non si riescono ad avere le risposte bisogna procedere nell'incertezza e affidarsi all'intuito.

## Perché lo facciamo

Questo è un test (abbastanza impegnativo) che facciamo per selezionare il nostro prossimo tirocinante.

Per noi il tirocinio è molto importante. Essendo ancora piccoli (attualmente in BitBoss lavorano 18 persone tra team Interno e developer esterni) la persona che selezioniamo ora potrebbe avere un impatto molto grande nel futuro, una volta integrata all'interno del Team.

Inoltre, essendo un contratto di formazione, per noi rappresenta un investimento importante in termini di tempo e risorse. Per questo motivo vogliamo selezionare le persone più capaci e motivate.

## Non puoi essere un Tirocinante?
Se non hai i requisiti per attivare un tirocinio, ma vuoi comunque metterti alla prova, saremo felici di valutare la tua candidatura.

